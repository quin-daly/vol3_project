# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Project Proposal (10/22/2021) ###

Our group consists of Kellon Sandall, Quin Daly, Kaden Call, Peter Fortuna, and Rachel Bennett. We will be investigating if pell grants are being distributed to those who need them most. We will also be analyzing how for-profit versus non-for-profit schools affect graduation rates. 

We will be using datasets from (government) and opportunity insights.org. One dataset measures the total Pell grant aid given to ~5000 colleges, and the other measures the mobility score of ~2000 colleges. We will combine these datasets by joining on then “College” column. Since one dataset has more points than the other, we will have to drop some of the data points from the college dataset. 

The features we intend to use are the total aid distributed as compared with mobility score. The mobility score describes the percentage of students with parents in the bottom 20% of incomes who move to the top 20% income.

Other features that we plan to look at are the demographics of the schools, including Asian/Pacific Islander population, Black population, and Hispanic population.

Data cleaning:
Although joining on the college name column should be simple, we will need to ensure that they match in convention (i.e. in one dataset it included a college called American Career College of Los Angeles, but the other simply calls it American Career College.) This way we will not be introducing false data points into our set. Likewise their ID numbers (ope_id) have trailing zeroes in one set but not the other. We will also be checking the data to ensure that the data is accurate, I.e. the total aid distributed is within a valid range.

Methods:
For the dataset we plan to use Random Forests and linear regression. For example, we will create a hypothetical school and measure how much Pell grant aid is predicted to be awarded to it.
Metrics:
P-value —the variance of the outcomes
We will also use the accuracy score of Random Forest
Identify variables that correlate most with receiving Pell Grants
We think it would be interesting to check if BYU is an outlier in regards to Pell Grants.

Dividing Responsibility:
Kellon: writing
Quin: regression
Kaden: cleaning
Peter: feature engineering
Rachel: visualizations/graphs

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact