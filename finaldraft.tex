\documentclass[11pt]{article}

\usepackage[top=1in,left=1in,right=1in,bottom=1in]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{enumitem}
\usepackage{verbatim}
\usepackage{slashed}
\usepackage{multicol}
\usepackage{url}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{hanging}

\usepackage{listings}
\usepackage{xcolor}

%New colors defined below
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

%Code listing style named "mystyle"
\lstdefinestyle{mystyle}{
	backgroundcolor=\color{backcolour}, commentstyle=\color{codegreen},
	keywordstyle=\color{magenta},
	numberstyle=\tiny\color{codegray},
	stringstyle=\color{codepurple},
	basicstyle=\ttfamily\footnotesize,
	breakatwhitespace=false, 
	breaklines=true, 
	captionpos=b, 
	keepspaces=true, 
	numbers=left, 
	numbersep=5pt, 
	showspaces=false, 
	showstringspaces=false,
	showtabs=false, 
	tabsize=2
}

%"mystyle" code listing set
\lstset{style=mystyle}

\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\II}{\mathbb{I}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\PS}{\mathcal{P}}


\begin{document}
	
	\vspace*{2cm}
	
	\begin{center}
		\LARGE Social and Educational Factors 
		
		that Influence the Distribution
		
		of Pell Grants
		
		\vspace{3cm}
		
		\Large 
		Kellon Sandall
		
		kgs256@byu.edu
		
		\vspace{24pt}
		
		Quin Daly
		
		quindaly@byu.edu
		
		\vspace{24pt}
		
		Peter Fortuna
		
		pjfort@byu.edu
		
		\vspace{24pt}
		
		Rachel Cardon
		
		rcardon8@byu.edu
		
		\vspace{24pt}
		
		Kaden Call
		
		ksc54@byu.edu
		
		\vspace{3cm}
		
		\normalsize
		
		\today
		
		Dr. Tyler Jarvis
		
		BYU, Mathematics
	\end{center}
	
	
	\pagebreak
	
	\normalsize
	
	\section{Introduction}
	
	At this moment, over 20 million people are attending US colleges. These students come from a wide variety of backgrounds, but they all expect college to provide them with opportunity and help them achieve financial stability. We use college level data to analyze how well Pell Grants are being distributed and how well they help students from poorer backgrounds achieve financial success.
	
	\section{Data Collection and Overview}
	
	The first part of our dataset combines data produced by Opportunity Insight and the US Department of Education. Opportunity Insight keeps track of various data about US colleges, such as the number of students enrolled at each college, acceptance rates, and cost of tuition. The Opportunity Insight data also contains information about racial demographics, university type (public, private, for profit) and the majors of students. What makes Opportunity Insight's dataset unique is the variables it contains about student and parent income. This dataset is one of the most prominent ways to gain insight into upwards mobility and opportunity at US colleges. It can help data scientists identify which colleges are serving the rich over the poor and majorities over minorities. Using tax records, the data scientists at Opportunity Insight were able to track the income of students and their parents before and after the student attended college. This allows for variables showing what percent of students at any given university come from wealthy backgrounds and what percent come from poor backgrounds. Several variables relate to how many students from poor families go on to become wealthy. As this data is all on a university level, it can be combined with information about the university to gain more general insights about upwards mobility and the opportunity college provides.
	
	Financial aid information was then added to our dataset. Every year, the US Department of Education gives Pell Grants, the foundation of a student's financial aid package, to students with financial need. The Department of Education keeps track of how many grants are given to each university and the total dollar amount awarded to each university. Students with more need receive a larger grant, with the money automatically being used to subsidize tuition. Any leftover funds can be used however the student chooses, and at no point in time do these funds need to be repaid. 
	
	While these datasets are a rich source of information, they do have limitations. Notably, Opportunity Insight data spans several years, and some variables are more recent than others, with most from the early 2000s and several from 2013 or 2014. In addition, the Opportunity Insight datasets have missing variables. This is addressed below. Income information in particular is missing for around ten percent of schools. Income information is originally extracted from tax records, and this process is imperfect, causing missing and occasionally incorrect variables. On top of this, tax records are noisy, imprecise records of income, meaning any given income extracted has random error to it. It should also be noted that these variables are the average of a sample instead of a universal average. A small percentage of the tax records from students at each university were obtained.
	
	It is also notable that both Opportunity Insight and Pell Grant data provide information at a college level instead of an individual level. This makes it harder to identify a direct relationship between a student's background, their college experience, and their financial outcomes. However, the dataset is still robust enough to provide insight into the opportunities colleges provide, as well as the effectiveness of Pell Grants. This approach also protects students' privacy and mitigates noise in the text extraction process.
	
	We expect this analysis to identify the traits of colleges with high upwards mobility, as measured by income. We expect that Pell Grants will contribute to upwards mobility.
	
	\section{Data Cleaning/ Feature Engineering}
	
	Data from Opportunity Insight have a common id (super\_opeid), so joining multiple tables is straightforward. We joined two tables, one with general college information and one with student income information. This join did result in some missing values, which are shown in this graph
	
	\begin{center}
		\includegraphics[width=6in]{Opportunity_Insight_Join.png}
	\end{center}
	
	In the graph above, we show what percent of observations are missing in the join between the two Opportunity Insight datasets. One dataset has features about general college statistics, and the other has features about student income and upwards mobility. The first dataset has at most two percent of any given variable missing after the join, while ten percent of the income data is missing. This is likely do to the difficulty of extracting income data from tax forms. These rates are relatively low, and we have not found any reason to believe they would bias the results.
	
	Adding data for Pell Grants involved more work. The main complication comes from the fact that data was added for twenty years, and each year has its own formatting and variable names. In addition, the id in the Pell Grants dataset was not the same id as in the Opportunity Insight dataset. The Opportunity Insight id was created using school names only, while the Pell Grant data is split up by location and school name. Hence, the Opportunity Insight IDs are a subset of the Pell Grant IDs. To join these two datasets, Pell Grant IDs were grouped to match the Opportunity Insight IDs and summed across these groups. In addition, the first three years of Pell Grant data was missing the IDs used in this join, so IDs from the following year were taken and added in using an alternate ID.
	
	\begin{center}
		\includegraphics{assessing_pell_grant.png}
	\end{center}
	
	Here, we have the percent of missing Pell Grant data for each year. The fewest observations are missing around 2010 and 2011, which is around the same time the Opportunity Insight data was prepared. As you move away from these years, missing data rates increase up to 10 percent, and for the earliest year, 18 percent. This means Pell Grant Data from 2010 and 2011 is most reliable, but we have found no reason to suspect the years with more missing variables introduce bias to the results.
	
	We inputted all missing Opportunity Insight observation with the mean for their respective columns, which could decrease model variance but allows us to keep all data points and is a standard practice. For Pell Grant data, we treated the schools like they received no Pell Grants each year they have missing data and replaced the missing observation with a 0. This is reasonable for schools which had not been founded and schools that went out of business. However, this will create bias when missing or misjoined data is the source of the missing observations. A more careful examination of the join would be required to determine which schools should be filled with 0 and which should be imputed with a mean.
	
	We created several features for the analysis below. To account for the variation in school sizes, we created variables for Pell Grants per student and Pell Grant dollars per student. We also added a variable, "squared sticker price", to use in an OLS regression, as the relationship in question seemed to be quadratic.
	
	\section{Robustness}
	
	The data cleaning code is written to easily add in more data from Opportunity Insight and to add in Pell Grant data from additional years. All data cleaning is done in a script. If more data from Opportunity Insight were added, this would be a straightforward join on college IDs.
	
	In addition, a single function is used to Pell Grant data. This function takes a filename, the name of the ID to merge on, and the variables to keep. It has several settings for various data specific nuances, and more could easily be added.
	
	\section{Which features are best at predicting the amount of Pell grant funds per student a school will receive?}
	
	To answer this question, the dependent variable that we were concerned with was rewards/recipients, focusing on the data from 2013. Thus, the independent variables were all other variables that weren't from the Pell grant dataset.
	
	\subsection{Methods}
	
	In order to analyze this question, we began by using the ordinary least squares (OLS) method. We started by dropping the non-numeric columns and other irrelevant features (data from earlier years, other Pell grant data, region codes, etc.) and ran the OLS model. We found that the condition number was very large (around 5.91e+09), which we believed had to do with the fact that there was large variation between the values of the different features. To lower this number, we divided each feature by its mean, forcing each value of the dataframe to be in a similar range without changing any of the OLS statistics.
	After making these changes, we began dropping features one-by-one from our model that had the highest p-values until we were left with a model containing only features that had p-values less than 0.05. This resulting model had an R-squared value of 0.572 and a condition number of 96.7, which was much lower than our initial attempt at fitting. We then plotted a couple of the model’s features along with their lines of best fit to better see the significance of our results.
	
	\begin{center}
		\includegraphics[width=5in]{q1b_health.jpg}
		
		\includegraphics[width=5in]{q1b_k_median.jpg}
		
		\includegraphics[width=5in]{q1b_mobility.jpg}
	\end{center}

	Then we tried out a Random Forest Regression on the data. We performed a grid search using different hyperparameters. We ended up with a random forest with 300 trees, unlimited depth, a maximum of 10 features selected per tree, and all other hyperparameters at default. The $R^2$ score for the model was .933. This could mean that the tree is good at describing the data, but also that it could be overfitting. Here is a graph which shows which features were most influential in the random forest.
	
	\includegraphics[width=7in]{random_forest_features.png}
	
	\subsection{Results and Conclusion}
	
	This model showed us that there are many features that can be useful to predict how much funding per student a school will receive in the form of pell grants. In particular, things like mobility rate, median income after graduation, and fraction of parents in the bottom 20\% of the income distribution all had a relatively high positive correlation with funds received. There were also some surprising negatively correlated features, like the share of health and medicine majors at a school. While these correlations could very well be accurate, they could also have been a result of the way we decided to handle missing data values from our original dataset.
	
	Based off of the $R^2$ scores, it appears that the random forest does a better job at predicting the average amount of Pell grant per student than the OLS method. However, more research could be done to determine if some of this was due to overfitting. It's also interesting how the features ipeds\_enrollment\_2013 and count were by far the two most influential features. More research could be done in this area. Overall, we were able to determine that there is some correlation between social and educational factors and the amount of Pell grant per student per school. 	
	
	\section{Is there a relation between for-profit schools and racial and economic factors?}
	
	\subsection{Introduction and Variables}
	There is the stereotype that for-profit schools take advantage of people. In this question we explore if there is a relation between if a school is a for-profit and the percent of students from certain races and income levels. In this section we use four different classification techniques: Naive Bayes, K-Nearest Neighbors, Logistic Regression, and Random Forests. For all of the models we use the sci-kit learn implementation. 
	
	We use the following features, percentage of students attending the school of the ethnicities: Asian and Pacific Islanders, Hispanic, Nonresident aliens, and Blacks. As well as Parents median income, students whose parents are in the bottom 20\% of the income distribution, and students whose parents are in the top 1\% of the income distribution to predict if a school is for-profit or public/private non-profit.
	
	\subsection{Model Results}
	
	The KNN model performed the worst of all the models resulting in only a 61\% accuracy suggesting that there is no form of proximity between the for-profit schools. Naive Bayes did not perform much better with a 66\% accuracy. The Logistic Regression performed better with an accuracy score 74\%. This is most likely because the data cannot be linearly separated very well. The Random Forest performed the best out of the three with an accuracy of 81\%. When looking at the ROC curve we see the same results with the Random Forest having a significantly higher AUC than the other models. In the following analysis we use the Random Forest model.
	
	\includegraphics{Kaden Roc Curve.png}
	
	\subsection{Feature Imortance}
	
	When looking at the importance of the features we notice that where the parents are in the income distribution is the most important. The percent of students that are Asian or Pacific Islander is the next most important feature followed closely by parents median income. The last three racial factors, Nonresident Alien, Hispanic, and Black are the least important factors. 
	
	\includegraphics[width=7in]{Kaden Feature Importance.png}
	
	\subsection{Conclusion}
	
	We found that we are able to reasonably predict wether a school is for-profit or non-profit based on a combination of economic and racial factors of the schools demographics. This suggests that there is a correlation between these factors and if a school is for-profit or non-profit. The parents economic status are the most important factors in determining what type of school it is. However some racial factors do have an impact.
	
	\section{What factors best predict the mobility rate for a given college?}
	
	\subsection{Explanation of Variables}
	We tested variables such as a school's graduation rate (the percent of students who graduated within 150\% of standard time, i.e. 6 years for a bachelor's degree), the sticker price of attending the school (measured in thousands of dollars), the amount of Pell grant distributed at the school per student (measured in thousands of dollars per student), and the school's percentage of majors in the following fields: Art, business, health, multidisciplinary, social sciences, STEM, and trade. 
	
	The dependent variable was the Mobility Rate, represented by mr\_kq5\_pq1. This is defined as the percent of students who reach the top 20\% of the income distribution, whose parents’ income was in the bottom 20\% of the distribution. 
	
	\subsection{Methods Used}
	We used the OLS methods from the Python statsmodels library. After loading in the independent and dependent variables, we iteratively removed two features with the highest p-values: multidisciplinary majors and health majors. We then analyzed the coefficients and the p-values to look for significant factors. 
	
	\subsection{Visualizations}
	
	\includegraphics[width=7in]{OLS-Rachel-Final.png}
	
	
	\subsection{Results}
	We found that three features were significant: the sticker price of attending the college, the percent of STEM majors, and the percent of Social Science majors, with p-values of .002, .000, and .049, respectively. The sticker price had a negative effect on the mobility rate, whereas the percent of STEM majors and Social Science majors had a positive effect. 
	
	These results imply that higher tuition rates hurt mobility. It also suggests that majoring in a STEM field or a Social Science field can help upward mobility. 
	
	It is important to note that there is a reasonable amount of error and bias in this model. For example, the sticker price is the listed price of tuition for a college—however, many students pay little to nothing of that price, since schools often give tuition discounts to students in need. Likewise, these are measurements of groups of individuals rather than individuals, so there is some bias. 
	
	Mobility is significant, since Pell grants are ideally to help those in lower income brackets to move to higher ones. Notably, the Pell grant per student variable was not significant, with a p-value of .377. 
	
	
	\section{Conclusion and Ethical Concerns}
	
    Many college students struggle to pay their tuition and other costs of attending University. Pell Grants are aimed at helping students in particular financial need. These grants are based on students’ and their parents’ incomes. Since the college that a student chooses to attend can have significant impact on their college experience and future income, we wanted to see how these factors interacted with Pell Grants. 
 	
    Our results indicated that certain attributes of a college can accurately predict the amount of Pell Grant funds per student distributed at a given college. We found that the median income after graduation, as well as the fraction of parents in the bottom quintile of income could predict Pell grant funds quite well. Something to consider here is that it is most likely difficult for the Department of Education to determine how to allocate Pell Grant funds. It is likely there are many Pell Grant recipients that are relatively financially secure, as well as individuals who are struggling financially but did not qualify. In our results, we found that having parents in a lower quintile of income positively affects the amount of Pell Grant distribution. This, at least, indicates that the grants are being distributed reasonably efficiently. However, parents’ income is not the only factor to consider in a given student’s financial need, as some students do not receive aid from their parents. 
	
    Another important issue is that of profit versus non-profit schools. There are some ethical concerns here because a for-profit school could try to take advantage of economically disadvantaged groups. We found that we could accurately predict a profit/non-profit school based on economic and racial factors. Parents’ income was the most important feature. 
	
    Ideally Pell Grants would help students from poorer backgrounds to attend college and move to a higher quintile of income. We found that the sticker price of a college negatively impacted mobility. These results seem to suggest that high tuition and other costs can hurt a student’s ability to do that. This raises concerns, since conceivably a student might attend a higher-cost university, believing that the increased costs will benefit them in the long run.
    
    We also found that schools with a high percentage of students in STEM and Social Science Majors had higher mobility rates. One concern with these results is that this could suggest that majoring in a STEM or Social Science field will be able to increase their mobility, but this might not necessarily be true. There could be measurement error, as noted previously. A certain major does not guarantee a path from the lower quintiles to the upper ones, and this data is aggregated. 
    
    In future studies we would recommend looking at Pell Grant data distributed by individuals rather than in aggregate. This would help to more accurately predict factors that contribute to needing financial aid in college. 

	\pagebreak
	
	\Large{\textbf{References}}
	
	\vspace{.2in}
	
	\normalsize
	\begin{hangparas}{.25in}{1}
		[1] Chetty, R., Friedman, J., Saez, E., Turner, N., \& Yagan, D. (2018, October 28). Mobility Report Cards: The role of colleges in intergenerational mobility. Opportunity Insights. Retrieved December 1, 2021, from \url{https://opportunityinsights.org/paper/mobilityreportcards/}. 
		
		[2] Turner, L. J. (2014, August 14). The Road to Pell is Paved with Good Intentions: The Economic Incidence of Federal Student Grant Aid. UMD Department of Economics. Retrieved December 1, 2021, from \url{http://econweb.umd.edu/~turner/Turner\_FedAidIncidence.pdf}. 
	\end{hangparas}


\end{document}
